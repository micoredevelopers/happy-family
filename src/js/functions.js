import $ from 'jquery';
import 'slick-slider/slick/slick';
import AOS from 'aos';
import { Fancybox } from "@fancyapps/ui";

$(document).ready(function () {
    const $page = $('html, body');
    const menu = $('#menu');
    const btn = $('#burger');
    const body = $('body');
    const modal = $('#modal');

    Fancybox.bind('[data-fancybox="Наши работы"]', {
        animated: true,
        closeButton: 'outside',
        Image: {
            fit: "contain",
        },
    });

    btn.click(function () {
        const btn = $(this);
        const body = $('body')
        const menu = $('#menu');
        btn.toggleClass('active');
        menu.toggleClass('open');
        body.toggleClass('lock');
    });

    const apartmentsSlider = $('.apartments__slider');
    const introSlider = $('.intro__slider');

    apartmentsSlider.slick({
        infinite: true,
        slidesToShow: 4,
        arrows: false,
        swipeToSlide: true,
        lazyLoad: 'ondemand',
        accessibility: true,
        variableWidth: true,
        dots: true,
        dotsClass: 'slider-dots',
        responsive: [
            {
                breakpoint: 1200,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1,
                    infinite: true,
                    dots: true
                }
            },
            {
                breakpoint: 992,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 2
                }
            },
        ]
    })

    introSlider.slick({
        infinite: true,
        fade: true,
        swipe: false,
        autoplay: 2000,
        speed: 2000,
        lazyLoad: 'ondemand',
        easing: 'easy',
        arrows: false,
    })

    $('a[href*="#"].anchor').click(function () {
        btn.removeClass('active');
        menu.removeClass('open');
        body.removeClass('lock')
        $page.animate({
            scrollTop: $($.attr(this, 'href')).offset().top - 100
        }, 500);
        return false;
    });


    $('.animated-title').each((index, title) => {
        const text = $(title).attr('data-word').split(' ');
        text.filter(word => (word !== '' && word !== '<br>') && word).forEach((word, i) => {
            $(title).append(`<span> <span data-aos="fade-up-mask" data-aos-delay="${i * 150}">${word}</span></span>`)
        })
    })

    const closeModal = (elem, modal) => {
        $(elem).on('click', (e) => {
            $(modal).removeClass('open')
            body.removeClass('lock')
        });
    }

    closeModal('#closePopup', '#modal')
    closeModal('#closeSuccess', '#success')

    $('.openModal').on('click', function () {

        modal.addClass('open');
        body.addClass('lock');
        if (menu.hasClass('open')) {
            menu.removeClass('open');
            btn.removeClass('active');
        }
    })
})

$(window).on('load', function () {
    AOS.init({
        delay: 0,
        offset: 0,
        once: true,
        duration: 1000,
        easing: 'show-easing',
        initClassName: 'aos-init',
        animatedClassName: 'aos-animate'
    })
})

function callForm(form) {
    const msg = form.serialize();
    $.ajax({
        type: 'POST',
        url: '/sendmail.php',
        data: msg,
        success: function () {
            $('#success').addClass("open");
            form[0].reset();
        },
        error: function (xhr, str) {
            alert('Возникла ошибка: ' + xhr.statusText);
        }
    });
}

$(document).ready(function () {
    $('#modalForm, #footerForm').on('submit', function (e) {
        e.preventDefault();
        callForm($(e.target));
    });
});
